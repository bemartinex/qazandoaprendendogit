// Exercicio 2 - Estrutura de decisão

// Percorrer uma lista de cidades, sendo elas: Sao Paulo, Rio de Janeiro, Florianopolis, Recife
// Para cada item verificar se a cidade Florianopolis está presente na lista
// Caso esteja, avisar
// Caso não seja o elemento procurado, avisar
// Numero da execução

//LOG

//Execução: 1
// Cidade: São Paulo
// Encontrado / Não encontrado
// ---------------------------------
// ...

const cidades = ['Sao Paulo', 'Rio de Janeiro', 'Florianopolis', 'Recife']

cidades.forEach((cidade, i) => {

    console.log('Execução: ', i + 1)
    console.log('Cidade: ', cidade)

    if(cidade == 'Florianopolis'){
        console.log('Status: Encontrado')
    } else{
        console.log('Status: Não encontrado')
    }
    
    console.log('---------------------------------')
});