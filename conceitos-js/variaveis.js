// variavel string
var nome = 'Eduardo'
nome = 'Pedro'

console.log(nome)

//variavel int
let idade = 20 
idade = 30

console.log(idade)

//constante
const cidade = 'Florianopolis'

console.log(cidade)

//variavel boleana
var maior_idade = false

console.log(maior_idade)

//lista
const paises = ['Brasil','Argentina', 'Alemanha']

console.log(paises)

//objeto
let devices = {
    nome:'iPhone14',
    preco: 8000,
    cores: {
        cor1: 'vermelho', 
        cor2: 'azul'
    }
}

console.log(devices)