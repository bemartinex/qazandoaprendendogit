// se - senão

/*
if (condicao)
    {
        
    }else {

    }
*/

/*
var nome = 'Eduardo'

if (nome == 'Eduardo')
    {
        console.log('Condição atendida!')
    }else {
        console.log('Condição não atendida!')
    }

*/

let idade = 30

if (idade > 30){
    console.log('Condição atendida!')
} else {
    console.log('Condição não atendida!')
}