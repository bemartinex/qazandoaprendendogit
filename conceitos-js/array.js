const paises = ['Brasil','Argentina', 'Alemanha']

console.log('Acessando o pais: ' + paises[1]) //acessando uma posicao do array

console.log('Tamanho do array: ' + paises.length) //tamanho do array

// Operacoes com o array

paises.push('USA') // Adiciona no final do array
paises.pop() // Remove ultimo elemento da lista
paises.unshift('Franca') // Adiciona no comeco da lista
paises.shift() // Remove primeiro elemento do array

console.log(paises)