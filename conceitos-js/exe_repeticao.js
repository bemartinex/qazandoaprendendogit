// Exercicio 1 - Estrutura de repetição

// Percorrer uma lista de nomes, contendo os nomes: Eduardo, Maria, Fernando, Joao e Francisco
// Numero da execução, começando em 1 
// Nome que está sendo executado
// Separadores 

// LOG

// Execução: 1 
// Nome: Eduardo
// --------------------
// Execução: 2 
// Nome: Maria
// --------------------
// Execução: 3
// Nome: Fernando
// --------------------
//...

const nomes = ['Eduardo', 'Maria', 'Fernando', 'Joao', 'Francisco']

nomes.forEach((element, i) => {

    console.log('Execução: ', i + 1)
    console.log('Nome: ' + element)
    console.log('--------')
});

/* 
for(let i = 1; i < nomes.length; i++) {
    console.log('Execução: ' + i)
    console.log('Nome: ' + nomes[i])
    console.log('--------')
}
*/